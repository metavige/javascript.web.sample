﻿define(['hbs!./hello'], function(template) {

    var sandbox;
    var View;
    return {
        type: 'Backbone',

        events: {
            'submit form': 'formSubmitted'
        },

        initialize: function () {
            View = this;
            sandbox = this.sandbox;
            this.render();
        },

        render: function() {
            this.html(template({ name: 'Ricky Chiang' }));
        },
        
        formSubmitted: function(event) {
            console.log(View.formSerialize());
            // sandbox.log(View.formserialize());
            return false;
        }
    };
});