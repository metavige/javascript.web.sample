﻿define(['handlebars', 'hbs', 'i18nprecompile', 'json2'], function (Handlebars) {

    return {
        name: 'The Handle of the Bars',
        
        initialize: function(app) {
            // var Handlebars = require('handlebars');
            // 新定義一種 Template 的類型，類型名稱為 hbs
            app.core.template.hbs = Handlebars;
        }
    };
});