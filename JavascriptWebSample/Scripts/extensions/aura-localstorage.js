define({

    name: "Backbone LocalStorage",

    initialize: function (app) {
        app.sandbox.data.Store = require('backbone').LocalStorage;
    }

});