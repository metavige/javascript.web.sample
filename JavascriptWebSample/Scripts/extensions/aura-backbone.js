﻿define(['backbone', 'backbone-syphon', 'backbone-datagrid', 'backbone-paginator'], function (Backbone, BackboneSyphon) {
    return function (app) {
        var _ = app.core.util._;
        var historyStarted = false;
        // var Backbone;
        // var BackboneSyphon;
        return {
            name: 'backbone extension',
            initialize: function (app) {
                // Backbone = require('backbone');
                // BackboneSyphon = require('backbone-syphon');

                app.core.mvc = Backbone;
                app.sandbox.mvc = Backbone;

                // 替 View 加入序列化與反序列化的 Helper 方法
                var newWidgetView = _.extend(Backbone.View.prototype, {

                    formSerialize: function () {

                        return BackboneSyphon.serialize(this);
                    },

                    formDeserialize: function (data) {

                        BackboneSyphon.deserialize(this, data);
                    }
                });

                app.core.registerWidgetType('Backbone', newWidgetView);
            },
            afterAppStart: function (app) {
                if (!historyStarted) {
                    _.delay(function () {
                        Backbone.history.start();
                    }, 200);
                }
            }
        }
    }
});