'use strict';

/* ========================================== 
 應用程式的入口，這邊會用來宣告 Web 的開始
 ========================================== */
require(['aura/aura'], 
    function (Aura) {

        window.aura = new Aura({ debug: true, logEvents: true });
        aura
            .use('extensions/aura-jquery')
            .use('extensions/aura-backbone')
            .use('extensions/aura-handlebars')
            .use('extensions/aura-localstorage')
            .start({ widgets: 'body' })
            .then(function () {
                console.log('Application Started! ');
            });
    //Backbone.history.start();
});